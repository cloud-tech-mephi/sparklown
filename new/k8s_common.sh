#!bin/bash

sudo apt-get update
sudo apt-get install docker.io
sudo systemctl enable docker
sudo systemctl start docker

sudo apt-get install curl
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add

sudo apt-add-repository "deb http://apt.kubernetes.io/ kubernetes-xenial main"

sudo apt-get install kubeadm kubelet kubectl
sudo apt-mark hold kubeadm kubelet kubectl

# В качестве первого параметра передаем имя хоста (master, slave, worker, ...)
sudo hostnamectl set-hostname $1
