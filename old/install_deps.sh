#!/bin/bash
sudo apt-get update
sudo apt install default-jdk scala git -y
java -version; javac -version; scala -version; git --version
sudo apt install cmdtest
wget https://dlcdn.apache.org/spark/spark-3.2.0/spark-3.2.0-bin-hadoop3.2.tgz
tar xvf spark-*.tgz
rm -rf spark-*.tgz
sudo mv spark-3.2.0-bin-hadoop3.2 /opt/spark

sudo apt-get update
sudo apt install default-jre

echo "export SPARK_HOME=/opt/spark" >> ~/.bashrc
echo "export PATH=$PATH:$SPARK_HOME/bin:$SPARK_HOME/sbin" >> ~/.bashrc
echo "export PYSPARK_PYTHON=/usr/bin/python3" >> ~/.bashrc

source ~/.bashrc
