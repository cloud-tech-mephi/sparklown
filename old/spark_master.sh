#!/bin/bash
cd /opt/spark/sbin/
sudo echo "SPARK_MASTER_HOST=$1" >> /opt/spark/conf/spark-env.sh
./start-master.sh
./start-worker.sh spark://$1:7077
cd ../bin/
./spark-shell --master spark://$1:7077

# Первым аргументом передаем в скрипт внутренний ip-адрес мастера (10.0.1.3)
